﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerRespawn : MonoBehaviour
{

    public bool hasDied;
    public static int livesLeft = 3;
    public static float currentTime = 0;
    public static float winTime = 0;
    public GameObject livesLeftUI;
    public GameObject timeUI;
    public GameObject winTimeUI;
 
    [SerializeField]
    private GameObject GameOver;

    [SerializeField]
    private GameObject GameWon;

    /*
     *  Use this for initialization; resets number of lives after restarting the game
     */
    void Start()
    {
        if(livesLeft == 0)
        {
            livesLeft = 3;
            currentTime = 0;
        }

        if(winTime != 0)
        {
            currentTime = 0;
        }

        hasDied = false;

    }

    /*
     *  Update is called once per frame;
     *  updates the number of lives remaining and calls the end game function upon reaching 0 lives,
     *  and also updates the current game time and time UI
     */ 
    void Update()
    {
        livesLeftUI.gameObject.GetComponent<Text>().text = ("Lives: " + livesLeft);

        currentTime += Time.deltaTime;
        timeUI.gameObject.GetComponent<Text>().text = ("Time: " + currentTime);

        if (gameObject.transform.position.y < -9)
        {
            hasDied = true;

        }
        if (hasDied == true)
        {
            if(livesLeft >= 1)
            {
                livesLeft -= 1;
            }
            
            if(livesLeft > 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            else
            {
                endGame();
            }
        }
    }

    /*
     *  Function to decrement the number of lives by 1
     */
    public void decrementLives()
    {
        livesLeft -= 1;
    }


    /*
     *  Function to detect collisions and respond to the type of object collided with
     *  
     *  @param trig     The game object that triggered the collision event
     */
    void OnTriggerEnter2D(Collider2D trig)
    {

        if (trig.gameObject.tag == "Enemy")
        {
            hasDied = true;
        }

        if (trig.gameObject.name == "levelTwoSegue")
        {
            SceneManager.LoadScene("Level2");
        }

        if (trig.gameObject.name == "levelThreeSegue")
        {
            SceneManager.LoadScene("Level3");
        }

        if (trig.gameObject.name == "bonusSegue")
        {
            SceneManager.LoadScene("Bonus");
        }

        if (trig.gameObject.name == "victorySegue")
        {
            winGame();
        }

    }

    /*
     *  Function to bring up the 'GameOver' UI upon game ending
     */
    public void endGame()
    {
        GameOver.SetActive(true);
    }

   /*
    *  Function to bring up the 'Victory' UI upon winning the game
    */
    public void winGame()
    {
        winTime = currentTime;
        winTimeUI.gameObject.GetComponent<Text>().text = (winTime + " seconds");
        GameWon.SetActive(true);
    }

}
