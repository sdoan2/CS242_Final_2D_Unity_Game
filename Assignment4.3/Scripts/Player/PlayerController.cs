﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Code used in this class was referenced from Unity tutorial: https://www.youtube.com/watch?v=hn9lkAua3Vk 

public class PlayerController : PhysicsObject {

    // Player Stat Variables
    public int health = 100;
    public int maxHealth = 100;
    public float jumpSpeed = 7;
    public float maxSpeed = 7;
    public int lives = 3;


    // Use this for initialization
    void Start () {
		
	}
	
    /*
     *  Replaces 'Update' method that is called every frame.
     *  This method uses user input to calculate the player's velocity every frame.
     *  Detects user input for horizontal movement as well as jumping.
     * 
     */
	protected override void ComputeVelocity()
    {
        PlayerRaycast();
        Vector2 move = Vector2.zero;
        move.x = Input.GetAxis("Horizontal");

        if(Input.GetButtonDown("Jump") && grounded)
        {
            velocity.y = jumpSpeed;
        }

        else if (Input.GetButtonUp("Jump"))
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f;
            }

        }

        targetVelocity = move * maxSpeed;

    }

    /*
     *  Function to destroy enemy upon jumping on it
     */
     void PlayerRaycast()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down);
        if(hit != null && hit.collider != null && hit.distance < 0.9f && hit.collider.tag == "enemy")
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 1000);
            Destroy(hit.collider.gameObject);
        }
        if(hit != null && hit.collider != null && hit.distance < 0.9f  && hit.collider.tag != "enemy")
        {
            grounded = true;
        }
    }



        /*
         *  Function to be called whenever the player takes damage from enemy. 
         *  Also checks if the player will die upon taking this damage.
         *  
         *  @param health       The current health of the player
         *  @param damage       The damage inflicted to the player
         *  @return             The remaining health of the player
         */
        public int takeDamage(int health, int damage)
    {
        int curr_health = health - damage;

        if(curr_health > 0)
        {
            return health - damage;
        }
        
        else
        {
            resetPlayer(lives, maxHealth);
            return maxHealth;
        }
    } 


    /*
     *  Function to be called when the player dies (reaches 0 health), and reduces the 
     *  number of lives of the player by 1 and resets their health to full.
     *  
     *  @param numLives     The number of lives the player has remaining
     *  @param maxHealth    The amount of health points to reset too after reviving
     */
    public void resetPlayer(int numLives, int maxHealth)
    {
        if(numLives > 1)
        {
            numLives -= 1;
            health = maxHealth;
        }
        
    }
}
