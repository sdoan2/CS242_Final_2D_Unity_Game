﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScript : MonoBehaviour {

    // Game variables
    public int level = 1;
    public int maxLevel = 3;
    public bool levelWon = false;
    public bool gameWon = false;
    public bool playerDied = false;
    public int score = 0;

    // Use this for initialization
    void Start() {

    }
	
    /*
     *  Continues updating the game frame until either the game is won or the 
     *  player has died.
     *  
     */
	void Update()
    {
        while(gameWon == false)
        {
            while(playerDied = false)
            {
                continue;
            }
        }

    }

    /*
     *  Function to increment the score; to be called whenever
     *  the player kills an enemy
     *  
     *  @param score        The current score
     *  @param points       The points to be added
     *  @return             The new total score
     * 
     */ 
     public int killedEnemy(int score, int points)
    {
        return score += points;
    }


    
}
