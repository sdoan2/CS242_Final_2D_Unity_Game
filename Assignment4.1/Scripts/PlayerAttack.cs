﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Code was referenced from tutorial found at https://www.youtube.com/watch?v=0BQNf8jdaQ4&t=307s 
public class PlayerAttack : MonoBehaviour {

	
	// Update is called once per frame
	void Update () {
        PlayerRaycast();
	}


    /*
     *  Shoots a ray down from the player to hit enemies upon jumping on them
     */
    void PlayerRaycast()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down);
        if(hit.distance < 0.5f && hit.collider.tag == "Enemy")
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 1000);
            Destroy(hit.collider.gameObject);
        }
    }

}
