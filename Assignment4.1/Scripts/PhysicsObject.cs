﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Code used in this class was referenced from Unity tutorial: https://www.youtube.com/watch?v=hn9lkAua3Vk 

public class PhysicsObject : MonoBehaviour {

    // Reference to game object's rigidbody
    protected Rigidbody2D rb2d;

    // Velocity of game object
    protected Vector2 velocity;

    // Ground angle variables
    public float minGroundNormalY = 0.65f;
    protected bool grounded;
    protected Vector2 groundNormal;
    public float gravityModifier = 1f; 

    // Horizontal variables
    protected Vector2 targetVelocity; 

    protected const float minMoveDistance = 0.001f;

    // Cast variables
    protected ContactFilter2D contactFilter;
    protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    protected const float shellRadius = 0.01f;
    protected List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(16);

    // Function to get reference to the game object's rigidbody component
    private void OnEnable()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    void Start () {

        contactFilter.useTriggers = false;
        contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        contactFilter.useLayerMask = true;
	}
	
	// Update is called once per frame; resets velocity to 0 and recalculates velocity
	void Update () {

        targetVelocity = Vector2.zero;
        ComputeVelocity();
	}
    
    protected virtual void ComputeVelocity()
    {

    }

    /*
     *  Method to calculate velocity and move direction; accounting
     *  for gravity and ground angles
     * 
     */
    private void FixedUpdate()
    {
        velocity += gravityModifier * Physics2D.gravity * Time.deltaTime;
        velocity.x = targetVelocity.x;  // set x velocity

        grounded = false;

        Vector2 deltaPosition = velocity * Time.deltaTime;

        Vector2 moveAlongGround = new Vector2(groundNormal.y, -groundNormal.x);

        Vector2 move = moveAlongGround * deltaPosition.x;

        Movement(move, false);  // test x move

        move = Vector2.up * deltaPosition.y;
 
        Movement(move, true);   // test y move
    }


    /*
     *  Method to actually perform the game object's movement;
     *  checks to see if the game object moved, and if so, check for
     *  collisions, and for each collision, check if the game object 
     *  landed on the ground, and finally sets the object's next move accordingly.
     *  
     *  @param move         The move to perform
     *  @param yMovement    Boolean indicating if the move involves a jump/fall (y-movement)
     */
    void Movement(Vector2 move, bool yMovement)
    {
        float distance = move.magnitude;

        if(distance > minMoveDistance)
        {
            int count = rb2d.Cast(move, contactFilter, hitBuffer, distance + shellRadius);
            hitBufferList.Clear();
            for(int i = 0; i < count; i++)
            {
                hitBufferList.Add(hitBuffer[i]);
            }

            for(int i = 0; i < hitBufferList.Count; i++)
            {
                Vector2 currentNormal = hitBufferList[i].normal;
                if(currentNormal.y > minGroundNormalY)
                {
                    grounded = true; 
                    if(yMovement)
                    {
                        groundNormal = currentNormal;
                        currentNormal.x = 0;
                    }
                }

                float projection = Vector2.Dot(velocity, currentNormal);
                if(projection < 0)
                {
                    velocity = velocity - projection * currentNormal;
                }

                float modifiedDistance = hitBufferList[i].distance - shellRadius;

                distance = modifiedDistance < distance ? modifiedDistance : distance;
            }
        }
        rb2d.position = rb2d.position + move.normalized * distance;

    }
}
