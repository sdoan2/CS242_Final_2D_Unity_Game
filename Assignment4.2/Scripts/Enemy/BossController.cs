﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossController : PhysicsObject
{

    // Boss GameObject variables
    public int xDir = 1;
    public float maxSpeed = 1.0f;
    public bool isDead = false;

    /*
     *  Replaces 'Update' method that is called every frame.
     *  This method uses user input to calculate the boss game object's velocity.
     * 
     */
    void Update()
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(xDir, 0) * maxSpeed;

    }

    /*
     *  Function to change (reverse) the direction of the boss game object's movement
     */
    void Flip()
    {
        maxSpeed = 5.0f;

        if (xDir > 0)
        {
            xDir = -1;
        }
        else
        {
            xDir = 1;
        }
    }


    /*
     *  Function to detect collisions with other game objects and respond if the collision is with a game object tagged with "Wall"
     *  
     *  @param trig     The game object that triggered the collision event
     */
    void OnTriggerEnter2D(Collider2D trig)
    {

        if (trig.gameObject.tag == "Wall")
        {
            Flip();
        }


    }

}
