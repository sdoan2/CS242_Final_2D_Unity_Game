﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyController : PhysicsObject
{

    // Player Stat Variables
    public int health = 500;
    public float maxSpeed = 0.3f;
    public bool isDead = false;

    /*
     *  Replaces 'Update' method that is called every frame.
     *  This method uses user input to calculate the player's velocity.
     * 
     */
    void Update()
    {
        targetVelocity = Vector2.right * maxSpeed;
        
    }

    
    /*
     *  Function to allow enemy to take damage; to be called whenever the player
     *  successfully attacks the enemy. Also checks if the enemy will die upon
     *  taking this damage.
     *  
     *  @param damage       The amount of damage to take
     */
    public void takeDamage(int damage)
    {
        health -= damage;

        if(health < 0)
        {
            isDead = true;
        }
    }
   
}
