﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SnowmanEnemyController : PhysicsObject
{

    // Snowman Enemy Variables
    public int xDir = 1;
    public float maxSpeed = 1.0f;
    public bool isDead = false;

    /*
     *  Replaces 'Update' method that is called every frame.
     *  This method uses user input to calculate the enemy's velocity.
     * 
     */
    void Update()
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(xDir, 0) * maxSpeed;

    }


    /*
     *  Function to flip x-direction of the enemy's movement
     */
    void Flip()
    {
        if (xDir > 0)
        {
            xDir = -1;
        }
        else
        {
            xDir = 1;
        }
    }

    /*
     *  Function to have enemy game object react to collisions with game objects tagged with "Crate"
     *  
     *  @param trig     The game object that triggered the collision event
     */
    void OnTriggerEnter2D(Collider2D trig)
    {

        if (trig.gameObject.tag == "Crate")
        {
            Flip();
        }


    }

}
