﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerRespawn : MonoBehaviour
{

    public bool hasDied;
    public static int livesLeft = 3;
    public GameObject livesLeftUI;
 
    [SerializeField]
    private GameObject GameOver;

    /*
     *  Use this for initialization; resets number of lives after restarting the game
     */
    void Start()
    {
        if(livesLeft == 0)
        {
            livesLeft = 3;
        }

        hasDied = false;

    }

    /*
     *  Update is called once per frame;
     *  updates the number of lives remaining and calls the end game function upon reaching 0 lives
     */ 
    void Update()
    {
        livesLeftUI.gameObject.GetComponent<Text>().text = ("Lives: " + livesLeft);

        if (gameObject.transform.position.y < -9)
        {
            hasDied = true;

        }
        if (hasDied == true)
        {
            if(livesLeft >= 1)
            {
                livesLeft -= 1;
            }
            
            if(livesLeft > 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            else
            {
                endGame();
            }
        }
    }

    /*
     *  Function to decrement the number of lives by 1
     */
    public void decrementLives()
    {
        livesLeft -= 1;
    }


    /*
     *  Function to detect collisions and respond to the type of object collided with
     *  
     *  @param trig     The game object that triggered the collision event
     */
    void OnTriggerEnter2D(Collider2D trig)
    {

        if (trig.gameObject.tag == "Enemy")
        {
            hasDied = true;
        }

        if (trig.gameObject.name == "levelTwoSegue")
        {
            SceneManager.LoadScene("Level2");
        }

        if (trig.gameObject.name == "levelThreeSegue")
        {
            SceneManager.LoadScene("Level3");
        }
    }

    /*
     *  Function to bring up the 'GameOver' UI upon game ending
     */
    public void endGame()
    {
        GameOver.SetActive(true);
    }

}
