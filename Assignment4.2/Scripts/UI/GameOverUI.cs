﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverUI : MonoBehaviour {

    /*
     *  Function to be called by on-click button event to restart the game at level 1
     */ 
    public void Retry()
    {
        SceneManager.LoadScene("2DPlatformerMain");
    }

}
